const productsData = [
{
	id: "shs001",
	name: "Air Jordan 6",
	description: "Retro, Men",
	price:10895,
	onOffer: true
},
{
	id: "shs002",
	name: "Air Jordan 5",
	description: "Retro SE, Men",
	price:11895,
	onOffer: true
},
{
	id: "shs003",
	name: "Air Jordan 2",
	description: "Retro, Men",
	price:10895,
	onOffer: true
},
]

export default productsData;