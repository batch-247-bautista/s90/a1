import { useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function UserSettings() {
  const { user } = useContext(UserContext);

  const handleFormSubmit = (event) => {
    event.preventDefault();

    // Handle form submission logic here
  }

  return (
    <Container>
      <h1>User Settings</h1>
      <p>User ID: {user.id}</p>
      <p>User Is Admin: {user.isAdmin ? 'Yes' : 'No'}</p>
      <Form onSubmit={handleFormSubmit}>
        <Form.Group controlId="formEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" />
        </Form.Group>

        <Form.Group controlId="formPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" />
        </Form.Group>

        <Button variant="primary" type="submit">
          Save Changes
        </Button>
      </Form>
    </Container>
  );
}
