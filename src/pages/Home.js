import Banner from '../components/Banner';
import Highlight from '../components/Highlight';

export default function Home(){
	const data = {
		title: "Shoes Mio",
		content: "Shoes for everyone, anytime",
		destination: "/products",
		label: "Order Now!"
	}
	return (
			<>
			<Banner data={data}/>
			<Highlight />
			</>
	)
}