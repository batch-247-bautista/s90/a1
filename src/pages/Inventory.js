import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';

function Inventory() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`https://capstone-00002.onrender.com/products/all`)
      .then(res => res.json())
      .then(data => setProducts(data))
      .catch(err => console.error(err));
  }, []);

  const updateProduct = (productId, isActive, isArchived) => {
    fetch(`https://capstone-00002.onrender.com/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({ isActive, isArchived })
    })
      .then(res => res.json())
      .then(data => {
        setProducts(prevProducts => {
          const index = prevProducts.findIndex(p => p._id === productId);
          if (index === -1) return prevProducts;
          const updatedProduct = { ...prevProducts[index], isActive, isArchived };
          return [
            ...prevProducts.slice(0, index),
            updatedProduct,
            ...prevProducts.slice(index + 1)
          ];
        });
        Swal.fire({
          icon: 'success',
          title: 'Archive Successful!',
          text: 'Product is now unavailable',
          confirmButtonText: 'OK'
        });
      })
      .catch(err => console.error(err));
  };

  const toggleProduct = (productId, isActive) => {
    const index = products.findIndex(p => p._id === productId);
    if (index === -1) return;
    const product = products[index];
    updateProduct(productId, !product.isActive, product.isArchived);
  };

  const archiveProduct = (productId) => {
    const index = products.findIndex(p => p._id === productId);
    if (index === -1) return;
    const product = products[index];
    updateProduct(productId, product.isActive, true);
  };

  const updateProductsDetails = (productId, name, description, price) => {
  const updates = { name, description, price };
  fetch(`http://localhost:4005/products/${productId}/update`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify(updates)
  })
    .then(res => res.json())
    .then(data => {
      // Update the local state to reflect the updated product
      setProducts(prevProducts => {
        const index = prevProducts.findIndex(p => p._id === productId);
        if (index === -1) return prevProducts;
        const updatedProduct = { ...prevProducts[index], name, description, price };
        return [
          ...prevProducts.slice(0, index),
          updatedProduct,
          ...prevProducts.slice(index + 1)
        ];
      });

      // Show a Swal to indicate that the update is successful
      Swal.fire({
        icon: 'success',
        title: 'Update Successful!',
        text: 'Product details updated',
        confirmButtonText: 'OK'
      });
    })
    .catch(err => console.error(err));
};


  return (
    <div>
      <h2>Inventory</h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Product ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Active</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map(product => (
            <tr key={product._id}>
              <td>{product._id}</td>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>{product.isActive ? 'Active' : 'Inactive'}</td>
              <td>
                <div className="d-flex flex-column">
                  <Button
                    variant="primary"
                    as={Link}
                    to={`/products/${product._id}`}
                    className="mb-2"
                  >
                    View
                  </Button>
                  <Button
                    variant="warning"
                    onClick={() => toggleProduct(product._id)}
                    className="mb-2"
                  >
                    {product.isActive ? 'Deactivate' : 'Activate'}
                  </Button>
                  <Button
                variant="info"
                as={Link}
                to={`http://localhost:4005/products/${product._id}/update`}
                className="mb-2"
              >
                Update
              </Button>
                  </div>
                </td>
            </tr>
            ))}
          </tbody>
      </Table>
    </div>
    );
  }

export default Inventory;






